export const nbsp = String.fromCharCode(
  160
);

export enum LessonType {
  fonts = 'fonts',
  colors = 'colors',
  animals = 'animals',
  composition = 'composition'
}
